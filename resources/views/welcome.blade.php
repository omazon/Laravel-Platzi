@extends('layouts.app')
@section('title','Bienvenida')
@section('content')
<div class="jumbotron text-center">
    <h1>Laratter</h1>
    <nav >
        <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="/acerca">Acerca de nosotros</a></li>
        </ul>
    </nav>
</div>
<div class="row">
    <form action="/message/create" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group @if($errors->has('message')) has-danger @endif">
            <input type="text" name="message" class="form-control" placeholder="¿Qué estas pensando?">
            @if($errors->has('message'))
                @foreach($errors->get('message') as $error)
                    <div class="form-control-feedback">{{$error}}</div>
                @endforeach
            @endif
            <input type="file" class="form-control-file" name="image">
        </div>
    </form>
</div>
    <div class="row">
        @forelse($messages as $message)
        <div class="col-6">
            @include('messages.message')
        </div>
            @empty
                <p>No hay mensajes</p>
        @endforelse
        @if(count($messages))
            <div class="mt-2 mx-auto">
                {{$messages->links('pagination::bootstrap-4')}}
            </div>
        @endif
    </div>
@stop