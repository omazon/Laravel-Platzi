<img src="{{$message->img}}" alt="" class="img-thumbnail">
<p class="card-text">
    <div class="text-muted">Escrito por: <a href="/{{$message->user->username}}">{{$message->user->name}}</a></div>
    {{$message->content}}
    <a href="/message/{{$message->id}}">Leer más</a>
</p>
<div class="card-text text-muted float-right">
    {{$message->created_at->diffForHumans()}}
</div>