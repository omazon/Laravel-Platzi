<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/acerca', 'PagesController@about');
Route::get('message/{message}','MessagesController@show');
Route::get('/messages','MessagesController@search');
Auth::routes();

Route::get('/auth/facebook','SocialAuthController@facebook');
Route::get('/auth/facebook/callback','SocialAuthController@callback');
Route::post('/auth/facebook/register','SocialAuthController@register');

Route::get('/home', 'HomeController@index');
Route::group(['middleware'=>'auth'],function(){
    Route::post('/{username}/dms','UsersController@sendPrivateMessage');
    Route::post('message/create','MessagesController@create');
    Route::post('/{username}/follow','UsersController@follow');
    Route::post('/{username}/unfollow','UsersController@unfollow');
    Route::get('/conversations/{conversation}','UsersController@showConversation');
});

Route::get('/{username}','UsersController@show');
Route::get('/{username}/follows','UsersController@follows');
Route::get('/{username}/followers','UsersController@followers');

Route::get('/api/message/{message}/responses','MessagesController@responses');